#!/bin/bash
# This script is used to perform very quick and dirty filesystem (for files created AFTER a certain date), and network/process evidence collection.
#
# THE EVIDENCE THIS SCRIPT COLLECTS SHOULD NOT BE CONSIDERED UNTAINTED.  THIS SCRIPT IS COLLECTING EVIDENCE USING TOOLS LOCATED ON A COMPROMISED/UNTRUSTED SYSTEM.
#
# TARGET_DATE format = YYYY-MM-DD hh:mm
TARGET_DATE="2020-01-01 23:00"
NOSHA1SUM=0
NOFIND=0
NONETSTAT=0
NOIP=0
NOHOSTNAME=0
WHICH="/usr/bin/which"

FIND=`${WHICH} find`
if [ -z "${FIND}" ]; then
        NOFIND=1
        echo "WARN: Find not found."
fi

SHA1SUM=`${WHICH} sha1sum`
if [ -z "${SHA1SUM}" ]; then
        NOSHA1SUM=1
        echo "WARN: sha1sum not found."
fi

NETSTAT=`${WHICH} netstat`
if [ -z "${NETSTAT}" ]; then
        NONETSTAT=1
        echo "WARN: netstat not found."
fi

HOSTNAME=`${WHICH} hostname`
if [ -z "${HOSTNAME}" ]; then
        NOHOSTNAME=1
        echo "WARN: hostname not found."
fi

IP=`${WHICH} ip`
if [ -z "${IP}" ]; then
        NOIP=1
        echo "WARN: ip not found."
fi

LS="/bin/ls"
DATE="/bin/date"

echo "=============================="
echo "Date info:"
${LS} -al ${DATE}
if [ ${NOSHA1SUM} == 0 ]; then
        ${SHA1SUM} ${DATE}
fi
${DATE}

echo
echo "=============================="
echo "LS info:"
${LS} -al ${LS}
if [ ${NOSHA1SUM} == 0 ]; then
        ${SHA1SUM} ${LS}
fi

if [ ${NOSHA1SUM} == 0 ]; then
        echo
        echo "=============================="
        echo "SHA1SUM info:"
        ${LS} -al ${SHA1SUM}
        if [ ${NOSHA1SUM} == 0 ]; then
                ${SHA1SUM} ${SHA1SUM}
        fi
        ${SHA1SUM} --version
fi

if [ ${NOIP} == 0 ]; then
        echo
        echo "=============================="
        echo "HOSTNAME info:"
        ${LS} -al ${HOSTNAME}
        if [ ${NOSHA1SUM} == 0 ]; then
                ${SHA1SUM} ${HOSTNAME}
        fi
        ${HOSTNAME}
fi

if [ ${NOIP} == 0 ]; then
        echo
        echo "=============================="
        echo "IP info:"
        ${LS} -al ${IP}
        if [ ${NOSHA1SUM} == 0 ]; then
                ${SHA1SUM} ${IP}
        fi
        ${IP} address
fi

if [ ${NONETSTAT} == 0 ]; then
        echo
        echo "=============================="
        echo "NETSTAT info:"
        ${LS} -al ${NETSTAT}
        if [ ${NOSHA1SUM} == 0 ]; then
                ${SHA1SUM} ${NETSTAT}
        fi
        ${NETSTAT} -tuanp
fi

if [ ${NOFIND} == 0 ]; then
        echo
        echo "=============================="
        echo "FIND info:"
        ${LS} -al ${FIND}
        if [ ${NOSHA1SUM} == 0 ]; then
                ${SHA1SUM} ${FIND}
        fi
        ${FIND} --version

        echo "=============================="
        ${FIND} / -type d \( -path /sys -o -path /proc \) -prune -false -o -newermt "${TARGET_DATE}" -ls 2> /dev/null
fi
